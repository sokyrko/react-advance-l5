import React, {useCallback} from 'react';
import   './styles.module.scss';

const Message = ({message, onLiked}) => {
    const { text, avatar, liked, createdAt } = message;
    const dateCreate = createdAt.substr(0,9)
    const handlePostLike =useCallback( ()=>{
        onLiked({...message, liked: !liked})
    },[liked,message,onLiked])

    return (
        <li className = "clearfix" >
            <div className = "message-data" >
                <span className = "message-data-time" >{ dateCreate}</span >
                <img src= {avatar} alt="avatar"/>
            </div >

            <div className = "message my-message" >{text}</div >
            { liked?
                <i className = "fa fa-thumbs-up liked" onClick = {handlePostLike} />
                :
                <i className = "fa fa-thumbs-down liked" onClick = {handlePostLike} />
            }
        </li >

    );
};

export default Message;