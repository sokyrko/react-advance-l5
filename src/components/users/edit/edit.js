import React from 'react';

const Edit = (add, edit, handleOnSave, handleOnClose,onUserAdd, onUserUpdate,
              handleChangeAvatar,handleChangeName,handleChangeEmail,handleChangeRole,handleChangePassword) => {

    return (
        <div className = "container" >
            <div id = "login-row" className = "row justify-content-center align-items-center" >
                <div id = "login-box" className = "col-md-12" >
                    <h3 className = "text-center text-info" >{add? 'Add user' : 'Edit users'}</h3 >
                    <form className = "form-group" onSubmit = {add? onUserAdd :onUserUpdate} >
                        <img src = {edit? edit.avatar: ''} alt = "avatar" />

                        <div className = "form-group" >
                            <input id = "avatar" className = "form-control" type = "file"
                                   placeholder = "Type name ..." value = { edit? edit.avatar: ''}
                                   onChange = {handleChangeAvatar} />
                        </div>
                        <div className = "form-group" >
                            <input id = "name" className = "form-control" type = "text"
                                   placeholder = "Type name ..." value = { edit? edit.name: ''}
                                   onChange = {handleChangeName} />
                        </div>
                        <div className = "form-group" >
                            <input id = "email" className = "form-control" type = "email"
                                   placeholder = "Type email ..." value = {edit ? edit.email : ''}
                                   onChange = {handleChangeEmail} />
                        </div>
                        <div className = "form-group" >

                            <input id = "role" className = "form-control" type = "text"
                                   placeholder = "Type role ..." value = {edit ? edit.role : ''}
                                   onChange = {handleChangeRole} />

                        </div>
                        <div className = "form-group" >
                            <input id = "password" className = "form-control" type = "text"
                                   placeholder = "Type password ..." value = {edit ? edit.password : ''}
                                   onChange = {handleChangePassword} />
                        </div>
                        <input type = "button" className = "text-info" onClick = {handleOnSave}
                               value = "Save" />
                        <input type = "button" className = "text-info" onClick = {handleOnClose}
                               value = "Close" />
                    </form >
                </div >
            </div >
        </div >
    );
};

export default Edit;