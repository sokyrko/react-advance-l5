/* eslint-disable */
import React, {useCallback, useEffect, useState} from 'react';
import {useSelector} from "react-redux";
import './styles.scss';
import {v4 as uuid} from "uuid";
import Segment from "../common/segment/segment";
import Modal from "../common/modal/modal";
import {messageActionCreator, userActionCreator} from "../../store/actions";
import { useDispatch} from 'react-redux';
// import {getUsers} from "../../store/user/actions";
import './styles.scss'

const User = () => {
    const {users}  = useSelector(state =>({ users: state.users.users }));
    const [edit, setEdit] = useState({ avatar:'', name:'', email:'', role:'', password:''});
    const [add, setAdd] = useState(false);
    const [opening, setOpening] = useState(false)
    const dispatch = useDispatch();
    const storage = window.sessionStorage;

    const handleOnClose = useCallback(  ()=>{
        setOpening(false)
    },[])


    const handleOnSave = useCallback(  ()=>{
        add? dispatch(userActionCreator.addUser({id:uuid(), ...edit}))
        // add? dispatch(userActionCreator.addUser({id: Math.random(10000)*1000, ...edit}))
           : dispatch(userActionCreator.updateUser(edit))
        setOpening(false)
        setAdd(false)
        setEdit(null)

    },[ add,edit,dispatch])

    const handleAdd = useCallback(()=>{
        setOpening(true)
        setAdd(true)
    },[])

    const handleUpdate = useCallback( (e)=>{
        setOpening(true)
        const id = e.target.getAttribute('data-edit-id');
        const fUser = users.find(user=> user.id === id)
            setEdit({...fUser})
    },[users])

    const handleDelete = useCallback((e) =>{
        const id = e.target.getAttribute('data-del-id')
        dispatch(userActionCreator.deleteUser(id))

    },[dispatch])

    const handleChangeName =  useCallback((e) =>{
        const value = e.target.value
        setEdit({...edit, name:value})
        },[edit])

    const handleChangeAvatar =  useCallback((e) =>{
        const file = e.target.files[0]
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = () => {
            setEdit({...edit, avatar: reader.result});
        }
        },[edit])

    const handleChangeEmail = useCallback((e) =>{
        const value = e.target.value
        setEdit({...edit, email:value})
    },[edit])

    const handleChangeRole = useCallback((e) =>{
        const value = e.target.value
        setEdit({...edit, role:value})
    },[edit])

    const handleChangePassword = useCallback((e) =>{
        const value = e.target.value
        setEdit({...edit, password:value})
    },[edit])


    const onUserAdd =useCallback(()=> {
        handleOnSave()
    },[handleOnSave])

    const onUserUpdate =useCallback(()=> {
        handleOnSave()
    },[handleOnSave])
useEffect(()=>{
    if (!storage.length) window.location.replace('http://localhost:3000/login')
    dispatch(userActionCreator.getUsers());
},[])

    return (
        <div >
                <div className = "container" >
                    <div id = "login-row" className = "row justify-content-center align-items-center" >
                            <div id = "login-box" className = " col-md-12" >
                                    <h3 className = "text-center text-info" >List users</h3 >
                                <ul className="ul-group">

                                    <li className=" list-decor-head  form-control">
                                        <span   className = "text-info span-start" >Avatar</span >
                                        <span   className = "text-info span-start" >Name</span >
                                        <span   className = "text-info span-middle" >Email</span >
                                        <span   className = "text-info span-end" >Role</span >
                                        <i className = "fa fa-user-plus" aria-hidden = "true" onClick={handleAdd}/>

                                    </li>
                                { users.map(user => {
                                        return <li  key={Math.random(1000)*1000} className = "form-control list-decor" >
                                            <img className = "text-info span-start img-responsive" src = {user.avatar} alt = "avatar" />
                                                <span   className = "text-info span-start" >{user.name}</span >
                                                <span   className = "text-info span-middle" >{user.email}</span >
                                                <span   className = "text-info span-end" >{user.role}</span >
                                            <div   className="spam-wrapper" >
                                                <i  className = "fa fa-edit" data-edit-id={user.id} onClick={handleUpdate}/>
                                                <i  className = "fa fa-trash" data-del-id={user.id} onClick={handleDelete}/>
                                            </div >
                                            </li>
                                    })
                                }
                                </ul>

                            </div >
                    </div >
                </div >
            <Modal
                isOpen = {opening}
                onClose = {handleOnClose}
                isCentered
            >
                <Segment >
                    <div className = "container" >
                        <div id = "login-row" className = "row justify-content-center align-items-center" >
                            <div id = "login-box" className = "col-md-12" >
                                <h3 className = "text-center text-info" >{add? 'Add user' : 'Edit users'}</h3 >
                                <form className = "form-group" onSubmit = {add? onUserAdd :onUserUpdate} >
                                    <img className = "text-info span-start img-responsive" src = {edit? edit.avatar:"https://i.stack.imgur.com/lAwxR.png"} alt = "avatar" />

                                    <div className = "form-group" >
                                        <input id = "avatar" className = "form-control" type = "file"
                                               onChange = {handleChangeAvatar} />
                                    </div>
                                    <div className = "form-group" >
                                    <input id = "name" className = "form-control" type = "text"
                                           placeholder = "Type name ..." value = { edit? edit.name: ''}
                                           onChange = {handleChangeName} />
                                    </div>
                                    <div className = "form-group" >
                                    <input id = "email" className = "form-control" type = "text"
                                           placeholder = "Type email ..." value = {edit ? edit.email : ''}
                                           onChange = {handleChangeEmail} />
                                    </div>
                                    <div className = "form-group" >
                                    <input id = "role" className = "form-control" type = "text"
                                           placeholder = "Type role ..." value = {edit ? edit.role : ''}
                                           onChange = {handleChangeRole} />
                                    </div>
                                    <div className = "form-group" >
                                    <input id = "password" className = "form-control" type = "text"
                                           placeholder = "Type password ..." value = {edit ? edit.password : ''}
                                           onChange = {handleChangePassword} />
                                    </div>
                                    <input type = "button" className = "text-info" onClick = {handleOnSave}
                                           value = "Save" />
                                           <input type = "button" className = "text-info" onClick = {handleOnClose}
                                           value = "Close" />
                                </form >
                            </div >
                        </div >
                    </div >
                </Segment >
            </Modal >
        </div >
    );
};

export default User;