import React from 'react';
import PropTypes from 'prop-types';
import {useSelector} from "react-redux";

const Header =({messagesCount, lastTime}) => {
    const usersCount = useSelector(state => state.users.count.num);

    return (
        <div >
            <span>{usersCount} participiants, </span>
            <span>{messagesCount} messages, </span>
            <span> last message at {lastTime} </span>
        </div >
    );
};

Header.propTypes = {
    countParticipants: PropTypes.string,
    countMessages : PropTypes.string,
    lastDate: PropTypes.string
};

export default Header;