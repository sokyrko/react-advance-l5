import React from 'react';
import {useSelector} from "react-redux";
import './styles.scss'
const Sidebar = () => {
    let messages  = useSelector(state => state.users.users );
    const  participants = Array.from(new Set(messages))
    const handleChange = (e)=>{
        // const search = e.target.value
    }
    return (
        <div id = "plist" className = "people-list" >
            <div className = "input-group" >
                <div className = "input-group-prepend" >
                    <span className = "input-group-text" ><i className = "fa fa-search" ></i ></span >
                </div >
                <input type = "text" className = "form-control" placeholder = "Search..." onChange={handleChange}/>
            </div >
            <ul className = "list-unstyled chat-list mt-2 mb-0" >
                {participants.map(elm => {
                    return (<li  key={elm.id} className = "clearfix" >
                        <img src = {elm.avatar} alt = "avatar" />
                        <div className = "about" >
                            <div className = "name" >{elm.user}</div >
                            <div className = "status" >
                                <i className = "fa fa-circle offline" ></i >
                                left 7 mins ago
                            </div >
                        </div >
                    </li >)
                })}
            </ul>
        </div>
    );
};

export default Sidebar;