import React , {useState} from 'react';
import { v4 as uuid } from 'uuid';
import {datetimeFormat} from '../common/helper/data.helper';
import './styles.module.scss'

const MessageInput = ({onAddMessage}) => {
    const [avatar, setAvatar]=useState(null);
    const [text, setText]=useState(null);
    const userId = window.sessionStorage.getItem('userId');
    const userName = window.sessionStorage.getItem('userName');
    const id = uuid();
    const handleText = (e) => {
        const text = e.target.value;
        setText(text);
    }
    const handleFile = (e) => {
        const file = e.target.files[0]
        const reader = new FileReader()
        reader.onload = () => setAvatar(reader.result)
        reader.readAsDataURL(file)
    }
const onSubmit = () =>{
    // const user = "Random Name";
    const createdAt = datetimeFormat();
    const editedAt = datetimeFormat();
    onAddMessage({ id, userId, avatar, user:userName, text , createdAt, editedAt,liked: false});
    setAvatar(null)
}
    return (


        <div   className = "chat-message fixed-bottom text-justify" >
            <div id="input-group" className = "input-group1 mr-5 pr-x-5 " >
                <div className = "input-group-prepend" >
                    <span className = "input-group-text" ><i className = "fa fa-send" ></i ></span >
                </div >
                <input type = "text" placeholder="Type text ..." onChange={handleText}/>
                <input type = "file" placeholder="Change avatar ..." onChange={handleFile} />

                <input  type="button" onClick={onSubmit} value="submit"/>
            </div >
        </div >


    );
};

export default MessageInput;