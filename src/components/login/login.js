// /* eslint-disable */
import React, {useCallback,  useState} from 'react';
import './styles.module.scss';
import {useSelector} from 'react-redux';
// import {Redirect} from 'react-router'

const Login = (onSubmit) => {
    const {users}  = useSelector(state =>({ users: state.users.users }));
    const [name, setName] = useState('')
    const [password, setPassword] = useState('')
    const storage = window.sessionStorage;
    storage.clear();
    const handleChangeName =useCallback((e) =>{
           const name = e.target.value;
           setName(name);
    },[])

    const handleChangePassword = useCallback((e) =>{
        const password = e.target.value;
            setPassword(password);
    },[])
    
     const handleSubmit = (e) => {
         e.preventDefault()
         const checkUser = users.find(user => (user.name === name && user.password === password))
         if (checkUser) {
             console.log('tru');
             if (checkUser.name === 'admin' && checkUser.password === 'admin') window.location.replace('/users')
             if (checkUser.role === 'user' || 'User') window.location.replace('/chat')
             storage.setItem('userId', checkUser.id)
             storage.setItem('userName', checkUser.user)
         }
     }

    return (
        <div id = "login" >
            <h3 className = "text-center text-white pt-5" >Login form</h3 >
            <div className = "container" >
                <div id = "login-row" className = "row justify-content-center align-items-center" >
                    <div id = "login-column" className = "col-md-6" >
                        <div id = "login-box" className = "col-md-12" >
                            <form id = "login-form" className = "form" onSubmit={handleSubmit}>
                                <h3 className = "text-center text-info" >Login</h3 >
                                <div className = "form-group" >
                                    <label htmlFor = "username" className = "text-info" >Username:</label ><br />
                                    <input type = "text" name = "username" id = "username" className = "form-control" onChange={handleChangeName}/>
                                </div >
                                <div className = "form-group" >
                                    <label htmlFor = "password" className = "text-info" >Password:</label ><br />
                                    <input type = "password" name = "password" id = "password" className = "form-control" onChange={handleChangePassword}/>
                                </div >
                                <div className = "form-group" >
                                    {/*<label htmlFor = "remember-me" className = "text-info" ><span >Remember me</span >*/}
                                    {/*    <span ><input id = "remember-me" name = "remember-me"*/}
                                    {/*                  type = "checkbox" /></span ></label ><br />*/}
                                    <input type = "submit" name = "submit" className = "btn btn-info btn-md"
                                           value = "submit" />
                                </div >
                                <div id = "register-link" className = "text-right" >
                                    {/*<Link to = "#" className = "text-info" >Register here</Link >*/}
                                </div >
                            </form >
                        </div >
                    </div >
                </div >
            </div >
        </div >
    );
};

export default Login;