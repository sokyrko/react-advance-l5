export const datetimeFormat = () => {
    const date = new Date();
    let dateTime = date.getFullYear() + "-" + date.getMonth() + 1
        + "-" + date.getDate()
        + "T" + date.getHours() + ":"
        + date.getMinutes() +":"
        + date.getSeconds() + "."
        +date.getTimezoneOffset().toString().substr(1)+"Z"
    //".936Z";
    // console.log(dateTime);
    return dateTime;
}