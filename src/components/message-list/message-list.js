import React, {useCallback, useEffect, useState} from 'react';
import Modal from '../common/modal/modal';
import Segment from '../common/segment/segment';
import TextArea from '../common/text-area/text-area';
import Message from '../Message/message';
import OwnMessage from '../own-message/own-message';
// import {v4 as uuid} from "uuid";
import {datetimeFormat} from '../common/helper/data.helper';
import style from './styles.scss';
import {useDispatch,useSelector} from "react-redux";
import {messageActionCreator} from "../../store/actions";

const MessageList = ({  onUpdate, onDelete, onLiked}) => {
    let messages  = useSelector(state => state.messages.chat.messages );
    const userId = window.sessionStorage.getItem('userId')
    const [isOpen, setIsOpen] = useState(false);
    const [editElm, setEditElm] = useState(null)
    const dispatch = useDispatch();
    const handleOnClose = () => {
        setIsOpen(false);
        onMessageUpdate();
    };

    const handleIsEdit = (elmEdit)=>{
        setIsOpen(true);
        setEditElm(elmEdit);
    }
    const handleDelete = (id) =>{
        onDelete(id);
    }
    const handleLiked = (id) => {
        onLiked(id)
    }
    const onMessageUpdate = () =>{
        const user = "Random Name";
        // const createdAt = datetimeFormat();
        const editedAt = datetimeFormat();
        return onUpdate({id: editElm.id, userId, avatar: editElm.avatar, user, text:editElm.text, createdAt: editElm.createdAt, editedAt:editedAt});
    }
    const onSaveText =  (e) => {
        e.preventDefault();
        const text = e.target.value;
        setEditElm({...editElm, text:text});
    } 

    const onSaveFile = (e) => {
        const file = e.target.files[0]
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = () => {
            setEditElm({...editElm, avatar: reader.result});
        }
    }
    const getMessages = useCallback(() => {
        dispatch(messageActionCreator.getMessages());
    }, [dispatch]);


    useEffect(() => {
        getMessages();
    }, [ dispatch, getMessages]);


    return (
        <div className={style.messageList}>

            <div className = "chat-history" >
                <ul className = "m-b-0" >

            {messages.map(message => {
                return  message.userId !==userId ?
                     (<Message key={Math.random(10000)*1000}  message ={message}
                                onLiked={handleLiked}/>)
                     :  (<OwnMessage key={Math.random(10000)*1000} message={message}
                                    isEdit={handleIsEdit} onDelete={handleDelete}/>)
                }

            )}
                </ul>
            </div>

            {/*{isOpen} */}
                <Modal
                    isOpen = {isOpen}
                    onClose = {handleOnClose}
                    isCentered
                >
                    <Segment >
                        <form onSubmit = {onMessageUpdate} >
                            <TextArea
                                name="areaTarget"
                                refs="areaTarget"
                                value = {editElm !== null ? editElm.text : ""}
                                onChange = {onSaveText}
                                placeholder = ""
                            />
                            <img src = {editElm !== null ? editElm.avatar : "https://i.stack.imgur.com/lAwxR.png"} alt = "avatar" />
                            <input id="change_avatar" type = "file" placeholder = "Type text ..." onChange = {onSaveFile} />
                            <input type = "button" value = "Submit" onClick = {handleOnClose} />
                        </form >
                    </Segment >
                </Modal >
            {/*:*/}
        </div >
    );
};

export default MessageList;