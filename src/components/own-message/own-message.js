import React from 'react';
import {datetimeFormat} from '../common/helper/data.helper';
import './styles.scss'

const OwnMessage = ({message,  isEdit, onDelete}) => {
    const {id, avatar, text, createdAt} = message;
    const dateCreate = createdAt.substr(0,9)

    const handleIsEdit = () =>{
        console.log('tttt');
        const timeUp = datetimeFormat();
        const message = {id, avatar, text: text, createdAt: timeUp, updatedAt: timeUp};
        isEdit(message);
    }
    const handleIsDelete = () =>{
        onDelete(id)
    }
    return (
        <li className = "clearfix" >
            <div className = "message-data text-right" >
                <span className = "message-data-time" >{dateCreate} </span >
                <img className="message-data-time float-right" src = {avatar} alt = "avatar" />
            </div >
            <div className = "message other-message float-right" >
                {/*<br/>*/}
                {/*<br/>*/}
                {text}
                <br/>
                <br/>
                <i  className = "fa fa-edit" data-edit-id={message.id} onClick={handleIsEdit}/>
                <i  className = "fa fa-trash" data-del-id={message.id} onClick={handleIsDelete}/>
                {/*<input  className="message-data-time float-right" type = "button" value="Edit" onClick={handleIsEdit}/>*/}
                {/*<input className="message-data-time float-right" type = "button" value="Delete" onClick={handleIsDelete}/>*/}

            </div >
        </li >


    );
};

export default OwnMessage;