import React from 'react';
import styles from './styles.scss';
import Spinner from '../common/spinner/spinner';
const Preloader = () => {
    return (
        <div className={styles.preloader}>
            <Spinner isOverflow={true}/>
        </div >
    );

};

export default Preloader;