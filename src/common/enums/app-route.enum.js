const AppRoute = {
  HOME: '/',
  LOGIN: '/login',
  CHAT: '/chat',
  USERS: '/users',
  USERS_EDIT: '/users/edit/:id',
};

export { AppRoute };
