const ContentType = {
  JSON: 'application/json',
  JPG: 'image/jpg',
  MULTIFORM: 'multipart/form-data'
};

export { ContentType };
