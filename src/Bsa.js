// import {useState} from "react";
import { Route, Routes } from 'react-router-dom';

// import {v4 as uuid} from 'uuid';
import Chat from './components/chat';
import './Bsa.css';
import Login from './components/login/login';
import {AppRoute} from './common/enums/app-route.enum';
import User from "./components/users/user";
import Edit from "./components/users/edit/edit";
import {useCallback, useEffect} from "react";
import {userActionCreator} from "./store/actions";
import {useDispatch} from 'react-redux';

const Bsa = () => {
    // const userId = {userId :uuid()};
    const dispatch = useDispatch();

    const getUsers = useCallback(() => {
        dispatch(userActionCreator.getUsers());
    }, [dispatch]);



    useEffect(() => {
        getUsers();
    }, [getUsers, dispatch]);
    return (
        <div className = "container" >
            <div className = "row clearfix" >
                <div className = "col-lg-12" >
                    <Routes>
                        <Route path={AppRoute.HOME} element={<Login />} />
                        <Route path={AppRoute.LOGIN} element={<Login />} />
                        <Route path={AppRoute.USERS} element={<User />} />
                        <Route path={AppRoute.USERS_EDIT} element={<Edit />} />
                        <Route path={AppRoute.CHAT} element={<Chat />} />
                    </Routes>
                </div>
            </div >
        </div >
    );
}

export default Bsa;
