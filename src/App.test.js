import { render, screen } from '@testing-library/react';
import Bsa from './App';

test('renders learn react link', () => {
  render(<Bsa />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
