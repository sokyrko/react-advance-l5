import {HttpMethod} from '../../common/enums/http-enums/http-method.enum';
import {ContentType} from '../../common/enums/http-enums/content-type.enum';

class Message {
  constructor({ apiPath, http }) {
    this._apiPath = apiPath;
    this._http = http;
  }

  getMessages() {
    return this._http.load(`${this._apiPath}/messages`, {
      method: HttpMethod.GET,
      headers :{
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    });
  }

  deleteMessage(id) {
    console.log(id);
    return this._http.load(`${this._apiPath}/messages/${id}`, {
      method: HttpMethod.DELETE,
      payload: JSON.stringify(id)

    });
  }

  updateMessage(payload) {
    const id = payload.id;
    return this._http.load(`${this._apiPath}/messages/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  addMessage(payload) {
    return this._http.load(`${this._apiPath}/messages`, {
      method:  HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }
  updateLike(payload) {
    return this._http.load(`${this._apiPath}/messages/${payload.id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

}
export { Message };
