import { User } from './user/user.service';
import { Message } from './message/message.service';
import { Http } from './http/http.service';
import {ApiPath} from '../common/enums/app-path.enum';
import { Storage } from './storage/storage.service';

const storage = new Storage({
  storage: localStorage
});

const http = new Http({
  storage
});

const user = new User({
  apiPath: ApiPath.API_PATH,
  http
});

const message = new Message({
  apiPath: ApiPath.API_PATH,
  http
});


export { http, storage, user, message };
