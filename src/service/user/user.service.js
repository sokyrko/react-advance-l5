import {HttpMethod} from '../../common/enums/http-enums/http-method.enum';
import {ContentType} from '../../common/enums/http-enums/content-type.enum';

class User {
  constructor({ apiPath, http }) {
    this._apiPath = apiPath;
    this._http = http;
  }
  
    getUsers() {
    return this._http.load(`${this._apiPath}/users`, {
      method: HttpMethod.GET,
       headers :{
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    });
  }

  deleteUser(id) {
    return this._http.load(`${this._apiPath}/users/${id}`, {
      method: HttpMethod.DELETE,
      payload: JSON.stringify(id)
    });
  }

  updateUser(payload) {
    const id = payload.id;
    return this._http.load(`${this._apiPath}/users/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  addUser(payload) {
    return this._http.load(`${this._apiPath}/users`, {
      method:  HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }
}
export { User };
