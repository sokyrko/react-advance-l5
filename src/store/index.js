import { configureStore } from '@reduxjs/toolkit';

import * as services from '../service/services';
import { userReducer, messageReducer } from './root-reducer';

const store = configureStore({
    reducer: {
        users: userReducer,
        messages: messageReducer

    }
    ,
    middleware: getDefaultMiddleware => (getDefaultMiddleware({
        thunk: {
            extraArgument: { services }
        }
    }))
});

export default store;