const ActionType = {
  GET_USERS: 'auth/get-all-users',
  ADD_USER: 'auth/add-user',
  UPDATE_USER: 'auth/update-user',
  DELETE_USER: 'auth/delete-user',
};

export { ActionType };

