import { createReducer } from '@reduxjs/toolkit';
import * as userActions from './actions';

const initialState = {
  users: [

  ],
  count:{num:0}
};

const userReducer = createReducer(initialState, builder => {

  builder.addCase(userActions.getUsers.fulfilled, (state, action) => {
    const { users } = action.payload;
    state.users= users;
    state.count.num = users.length;

  });

  builder.addCase( userActions.addUser.fulfilled, (state, action) => {
    const  user  = action.payload.payload;
    const lastId = state.users.reduce((res, elm)=>{
      if(res< elm.id ) res = elm.id
        return res
    },1)

    state.users.push({id:lastId + 1, ...user});
  });

  builder.addCase(userActions.updateUser.fulfilled, (state, action) => {
    const  user  = action.payload.payload;
    const id = user.id;
    Object.keys(state.users).map(key=>  state.users[key].id === id? state.users[key]=user: '')
  });

  builder.addCase(userActions.deleteUser.fulfilled, (state, action) => {
    const id  = action.payload;
    // console.log(id);
    let upUser = state.users.filter(fUser=>  fUser.id!== id.userId)
    state.users = upUser
  });

});

export { userReducer };
