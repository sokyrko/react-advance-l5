import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';
import * as services from '../../service/services';



const getUsers = createAsyncThunk(
  ActionType.GET_USERS,
  async () => {
    const users =  await services.user.getUsers();
    return { users };
  }
);

const deleteUser = createAsyncThunk(
  ActionType.DELETE_USER,
  async (userId ) => {
    await services.user.deleteUser(userId);
    return { userId };
  }
);

const updateUser = createAsyncThunk(
  ActionType.UPDATE_USER,
  async (payload ) => {
      console.log(payload);
    await services.user.updateUser(payload);
    return {payload };
  }
);

const addUser = createAsyncThunk(
  ActionType.ADD_USER,
  async (payload) => {
    await services.user.addUser(payload);
    return { payload };
  }
);

export {
  getUsers,
  updateUser,
  deleteUser,
  addUser
};
