const ActionType = {
  GET_MESSAGES: 'auth/get-all-messages',
  ADD_MESSAGE: 'auth/add-message',
  ADD_LIKE: 'auth/set-like',
  UPDATE_LIKE: 'auth/update-like',
  GET_LIKES: 'auth/get-likes',
  UPDATE_MESSAGE: 'auth/update-message',
  DELETE_MESSAGE: 'auth/delete-message',
};

export { ActionType };

