import { createReducer } from '@reduxjs/toolkit';
import * as messageActions from './actions';

const initialState = {
  chat: {
    messages:[
    ],
    "editModal": false,
    "preloader": true,
  },
};

const messageReducer = createReducer(initialState, builder => {
  builder.addCase(messageActions.getMessages.fulfilled, (state, action) => {
    const { messages } = action.payload;
    state.chat.messages= messages;

    state.chat.preloader = false;
    state.chat.editModal = true;
  });

  builder.addCase(messageActions.addMessage.fulfilled, (state, action) => {
    const message  = action.payload.payload;
    state.chat.messages.push(message);
  });

  builder.addCase(messageActions.updateMessage.fulfilled, (state, action) => {
    const  upMessage  = action.payload.payload;
    const store = state.chat.messages;
        Object.keys(store).forEach(key=>{
          store[key] = store[key].id===upMessage.id ?  upMessage: store[key];
        })
  });

  builder.addCase(messageActions.updateLike.fulfilled, (state, action) => {
    const  like  = action.payload.payload;
    const store = state.chat.messages;
    Object.keys(store).forEach(key=>{
      if( store[key].id===like.id)   store[key].liked = like.liked;
    })
  });
  builder.addCase(messageActions.deleteMessage.fulfilled, (state, action) => {
    const { messageId } = action.payload;
    console.log(messageId);
    console.log(action.payload.id);
    let messages = state.chat.messages.filter(fMess=> fMess.id!==messageId)
    state.chat.messages = messages
  });

});

export { messageReducer };
