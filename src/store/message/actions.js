import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';
import * as services from '../../service/services';

const getMessages = createAsyncThunk(
    ActionType.GET_MESSAGES,
    async () => {
        const messages =  await services.message.getMessages();
        return { messages };
    }
);

const deleteMessage = createAsyncThunk(
    ActionType.DELETE_MESSAGE,
    async (messageId ) => {
        await services.message.deleteMessage(messageId);
        return { messageId };
    }
);

const updateMessage = createAsyncThunk(
    ActionType.UPDATE_MESSAGE,
    async (payload ) => {
        await services.message.updateMessage(payload);
        return {payload };
    }
);

const addMessage = createAsyncThunk(
    ActionType.ADD_MESSAGE,
    async (payload) => {
        await services.message.addMessage(payload);
        return { payload };
    }
);
const updateLike = createAsyncThunk(
    ActionType.UPDATE_LIKE,
    async (payload ) => {
        await services.message.updateLike(payload);
        return {payload };
    }
);

export {
    getMessages,
    updateMessage,
    deleteMessage,
    addMessage,
    updateLike
};
